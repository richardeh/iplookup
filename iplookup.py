"""
iplookup.py
Get whois information from an IP address

Author: Richard Harrington
Date Created: 10/18/2013
"""

import socket

address=input("Input a hostname:\n")
try:
    print(socket.gethostbyname(address))
    print(socket.getaddrinfo(address,80))
except socket.herror as e:
    print(e)
